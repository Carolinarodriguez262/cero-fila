﻿namespace CeroFilas.Web.ViewModels.Common.Pagination
{
    public static class PageSizesConstants
    {
        public const int Partners = 8;

        public const int BlogPosts = 1;
    }
}
