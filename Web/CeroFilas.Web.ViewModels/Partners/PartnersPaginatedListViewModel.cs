﻿namespace CeroFilas.Web.ViewModels.Partners
{
    using CeroFilas.Web.ViewModels.Common.Pagination;

    public class PartnersPaginatedListViewModel
    {
        public PaginatedList<PartnerViewModel> Partners { get; set; }
    }
}
